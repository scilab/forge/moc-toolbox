// -------------------------------------------------------------------------
// moc - Matlab/Octave Compatibility toolbox
// Copyright (C) 2010-2014  Holger Nahrstaedt
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//-------------------------------------------------------------------------
//
//  <-- NO CHECK ERROR OUTPUT -->

// # data from Hocking, RR, "Methods and Applications of Linear Models"
temperature=[40;40;40;45;45;45;50;50;50;55;55;55;60;60;60;65;65;65];
strength=[66.3;64.84;64.36;69.70;66.26;72.06;73.23;71.4;68.85;75.78;72.57;76.64;78.87;77.37;75.94;78.82;77.13;77.09];
[p,s] = moc_polyfit(temperature,strength,1);
[y,dy] = moc_polyconf(p,40,s,0.05,'ci');
assert_checkalmostequal([y,dy],[66.15396825396826,1.71702862681486],200*%eps);
[y,dy] = moc_polyconf(p,40,s,0.05,'pi');
assert_checkalmostequal(dy,4.45345484470743,200*%eps);
