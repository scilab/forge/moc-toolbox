// -------------------------------------------------------------------------
// moc - Matlab/Octave Compatibility toolbox
// Copyright (C) 2010-2014  Holger Nahrstaedt
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//-------------------------------------------------------------------------
//
//  <-- NO CHECK ERROR OUTPUT -->
clear __xassert;
function t = __xassert(a,b,tol)
[nargout,nargin]=argn(0);
 if (nargin == 1)
   t = and(a(:));
 else
   if (nargin == 2)
     tol = 0;
   end
   if (or (size(a) ~= size(b)))
     t = 0;
   elseif (or (abs(a(:) - b(:)) > tol))
     t = 0;
   else
     t = 1;
   end
 end
endfunction
//
//test
//
 i = 0;
 t = [];
 r = [0:100];                        // original vector
 w = r - 2*%pi*floor((r+%pi)/(2*%pi));  // wrapped into [-%pi,%pi]
 tol = 1e3*%eps;                      // maximum expected deviation
 t(1) = __xassert(r, moc_unwrap(w), tol);               //moc_unwrap single row
 t(2) = __xassert(r', moc_unwrap(w'), tol);             //moc_unwrap single column
 t(3) = __xassert([r',r'], moc_unwrap([w',w']), tol);   //moc_unwrap 2 columns
 t(4) = __xassert([r;r], moc_unwrap([w;w],[],2), tol);  //check that dim works
 t(5) = __xassert(r+10, moc_unwrap(10+w), tol);         //check r(1)>%pi works
//
 t(6) = __xassert(w', moc_unwrap(w',[],2));  //moc_unwrap col by rows should not change it
 t(7) = __xassert(w, moc_unwrap(w,[],1));    //moc_unwrap row by cols should not change it
 t(8) = __xassert([w;w], moc_unwrap([w;w])); //moc_unwrap 2 rows by cols should not change them
//
//// verify that setting tolerance too low will cause bad results.
 t(9) = __xassert(or(abs(r - moc_unwrap(w,0.8)) > 100));
//
 assert_checktrue(and(t));
//
//test
 A = [%pi*(-4), %pi*(-2+1/6), %pi/4, %pi*(2+1/3), %pi*(4+1/2), %pi*(8+2/3), %pi*(16+1), %pi*(32+3/2), %pi*64];
 assert_checkequal (moc_unwrap(A), moc_unwrap(A, %pi));
 assert_checkequal (moc_unwrap(A, %pi), moc_unwrap(A, %pi, 2));
 assert_checkequal (moc_unwrap(A', %pi), moc_unwrap(A', %pi, 1));
//
//test
 A = [%pi*(-4); %pi*(2+1/3); %pi*(16+1)];
 B = [%pi*(-2+1/6); %pi*(4+1/2); %pi*(32+3/2)];
 C = [%pi/4; %pi*(8+2/3); %pi*64];
 D = [%pi*(-2+1/6); %pi*(2+1/3); %pi*(8+2/3)];
 E(:, :, 1) = [A, B, C, D];
 E(:, :, 2) = [A+B, B+C, C+D, D+A];
 F(:, :, 1) = [moc_unwrap(A), moc_unwrap(B), moc_unwrap(C), moc_unwrap(D)];
 F(:, :, 2) = [moc_unwrap(A+B), moc_unwrap(B+C), moc_unwrap(C+D), moc_unwrap(D+A)];
 assert_checkequal (moc_unwrap(E), F);
//
//test
 A = [0, 2*%pi, 4*%pi, 8*%pi, 16*%pi, 65536*%pi];
 B = [%pi*(-2+1/6), %pi/4, %pi*(2+1/3), %pi*(4+1/2), %pi*(8+2/3), %pi*(16+1), %pi*(32+3/2), %pi*64];
 assert_checkequal (moc_unwrap(A), zeros(1, length(A)));
 assert_checkequal (diff(moc_unwrap(B), 1)<2*%pi, ones(1, length(B)-1)==1);
//
//error moc_unwrap()
