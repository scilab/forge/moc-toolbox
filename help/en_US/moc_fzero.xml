<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from moc_fzero.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="moc_fzero" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>moc_fzero</refname>
    <refpurpose>solves the scalar nonlinear equation such that F(X) == 0</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [X, FX, INFO] = moc_fzero (FCN, APPROX, OPTIONS)
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>FCN :</term>
      <listitem><para> function name as string</para></listitem></varlistentry>
   <varlistentry><term>OPTIONS is a structure, with the following fields:</term>
      <listitem><para> </para></listitem></varlistentry>
   <varlistentry><term>'abstol' :</term>
      <listitem><para> absolute (error for Brent's or residual for fsolve) tolerance. Default = 1e-6.</para></listitem></varlistentry>
   <varlistentry><term>'reltol' :</term>
      <listitem><para> relative error tolerance (only Brent's method). Default = 1e-6.</para></listitem></varlistentry>
   <varlistentry><term>'prl' :</term>
      <listitem><para> print level, how much diagnostics to print. Default = 0, no diagnostics output</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Given Func, the name of a function of the form `F (X)', and an initial
approximation APPROX, `fzero' solves the scalar nonlinear equation such that
`F(X) == 0'. Depending on APPROX, `fzero' uses different algorithms to solve
the problem: either the Brent's method or the Powell's method of `fsolve'.
The computed approximation to the zero of FCN is returned in X. FX is then equal
to FCN(X). If the iteration converged, INFO == 1. If Brent's method is used,
and the function seems discontinuous, INFO is set to -5. If fsolve is used,
INFO is determined by its convergence.
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
moc_fzero('sin',[-2 1])
[x, fx, info] = moc_fzero('sin',-2)
options.abstol = 1e-2; moc_fzero('sin',-2, options)
   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>H. Nahrstaedt - Aug 2010</member>
   <member>Copyright (C) 2004 Lukasz Bodzon</member>
   </simplelist>
</refsection>
</refentry>
