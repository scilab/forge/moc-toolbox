// -------------------------------------------------------------------------
// moc - Matlab/Octave Compatibility toolbox
// Copyright (C) 2010-2014  Holger Nahrstaedt
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//-------------------------------------------------------------------------
//
//  <-- NO CHECK ERROR OUTPUT -->
//test
 r = 0:10:50;
 p = moc_poly (r);
 p = p / max (abs (p));
 x = linspace (0,50,11);
 y = moc_polyval (p,x) + 0.25*sin (100*x);
 [pf, s] = moc_polyfit (x, y, length (r));
 [y1, delta] = moc_polyval (pf, x, s);
 expected = [0.37235, 0.35854, 0.32231, 0.32448, 0.31328, ...
    0.32036, 0.31328, 0.32448, 0.32231, 0.35854, 0.37235];
    assert_checkalmostequal (delta, expected,%eps, 0.00001);
//
///test
 x = 10 + (-2:2);
 y = [0, 0, 1, 0, 2];
 p = moc_polyfit (x, y, length (x) - 1);
 [pn, s, mu] = moc_polyfit (x, y, length (x) - 1);
 y1 = moc_polyval (p, x);
 yn = moc_polyval (pn, x, [], mu);
 assert_checkalmostequal (y1, y, %eps,sqrt (%eps));
 assert_checkalmostequal (yn, y, %eps,sqrt (%eps));
//
//test
 p = [0, 1, 0];
 x = 1:10;
 assert_checkalmostequal (x, moc_polyval (p,x), %eps, %eps);
 x = x(:);
 assert_checkalmostequal (x, moc_polyval (p,x), %eps, %eps);
 x = matrix (x, [2, 5]);
 assert_checkalmostequal (x, moc_polyval (p,x), %eps, %eps);
 x = matrix (x, [5, 2]);
 assert_checkalmostequal (x, moc_polyval (p,x), %eps, %eps);
 x = matrix (x, [1, 1, 5, 2]);
 assert_checkalmostequal (x, moc_polyval (p,x), %eps, %eps);
//
//test
 p = [1];
 x = 1:10;
 y = ones ( (x));
 assert_checkalmostequal (y, moc_polyval (p,x), %eps, %eps);
 x = x(:);
 y = ones ( (x));
 assert_checkalmostequal (y, moc_polyval (p,x), %eps, %eps);
 x = matrix (x, [2, 5]);
 y = ones ( (x));
 assert_checkalmostequal (y, moc_polyval (p,x), %eps, %eps);
 x = matrix (x, [5, 2]);
 y = ones ( (x));
 assert_checkalmostequal (y, moc_polyval (p,x), %eps, %eps);
 x = matrix (x, [1, 1, 5, 2]);
