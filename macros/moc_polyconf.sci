function [y,dy] = moc_polyconf(p,x,varargin)
// Produce prediction intervals for the fitted y
// Calling Sequence
// [y,dy] = moc_polyconf(p,x,s)
// Description
// Produce prediction intervals for the fitted y. The vector p
// and structure s are returned from polyfit or wpolyfit. The
// x values are where you want to compute the prediction interval.
//
// moc_polyconf(...,['ci'|'pi'])
//
// Produce a confidence interval (range of likely values for the
// mean at x) or a prediction interval (range of likely values
// seen when measuring at x). The prediction interval tells
// you the width of the distribution at x. This should be the same
// regardless of the number of measurements you have for the value
// at x. The confidence interval tells you how well you know the
// mean at x. It should get smaller as you increase the number of
// measurements. Error bars in the physical sciences usually show
// a 1-alpha confidence value of erfc(1/sqrt(2)), representing
// one standandard deviation of uncertainty in the mean.
//
// moc_polyconf(...,1-alpha)
//
// Control the width of the interval. If asking for the prediction
// interval 'pi', the default is .05 for the 95% prediction interval.
// If asking for the confidence interval 'ci', the default is
// erfc(1/sqrt(2)) for a one standard deviation confidence interval.
//
// Examples
// x=[40;40;40;45;45;45;50;50;50;55;55;55;60;60;60;65;65;65];
// y=[66.3;64.84;64.36;69.70;66.26;72.06;73.23;71.4;68.85;75.78;72.57;76.64;78.87;77.37;75.94;78.82;77.13;77.09];
// [p,s] = moc_polyfit(x,y,1);
// xf = linspace(x(1),x($),150);
// [yf,dyf] = moc_polyconf(p,xf,s,'ci');
// subplot(211)
// plot(xf,yf,'g-',xf,yf+dyf,'g.',xf,yf-dyf,'g.',x,y,'xr');
// subplot(212)
// plot(x,y-moc_polyval(p,x),'b.',xf,dyf,'g-',xf,-dyf,'g-');
// Authors
// Paul Kienzle
// Holger Nahrstaedt - 2015

// This program is granted to the public domain.
[nargout,nargin]=argn(0);

    alpha = [];
    s = [];
    typestr = 'pi';
    for i=1:length(varargin)
      v = varargin(i);
      if isstruct(v) then
          s = v;
      elseif type(v)==10 then
          typestr = v;
      elseif isscalar(v) then
          alpha = v;
      else s = [];
      end
    end
    if (nargout>1 & (isempty(s)|nargin<3)) | nargin < 2
      error("[y,dy] = moc_polyconf(p,x,s)");
    end
    if isempty(s)
      y = moc_polyval(p,x);
    else
      // For a polynomial fit, x is the set of powers ( x^n ; ... ; 1 ).
      n=length(p)-1;
      k=length(x(:));
      if moc_columns(s.R) == n,// fit through origin
        A = (x(:) * ones (1, n)) .^ (ones (k, 1) * (n:-1:1));
        p = p(1:n);
      else
        A = (x(:) * ones (1, n+1)) .^ (ones (k, 1) * (n:-1:0));
      end
      y = x;
      dy = x;
      [y(:),dy(:)] = moc_confidence(A,p,s,alpha,typestr);
    end
    endfunction
    //test
    // # data from Hocking, RR, "Methods and Applications of Linear Models"
    // temperature=[40;40;40;45;45;45;50;50;50;55;55;55;60;60;60;65;65;65];
    // strength=[66.3;64.84;64.36;69.70;66.26;72.06;73.23;71.4;68.85;75.78;72.57;76.64;78.87;77.37;75.94;78.82;77.13;77.09];
    // [p,s] = polyfit(temperature,strength,1);
    // [y,dy] = polyconf(p,40,s,0.05,'ci');
    // assert([y,dy],[66.15396825396826,1.71702862681486],200*eps);
    // [y,dy] = polyconf(p,40,s,0.05,'pi');
    // assert(dy,4.45345484470743,200*eps);
