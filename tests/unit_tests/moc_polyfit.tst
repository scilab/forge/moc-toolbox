// -------------------------------------------------------------------------
// moc - Matlab/Octave Compatibility toolbox
// Copyright (C) 2010-2014  Holger Nahrstaedt
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//-------------------------------------------------------------------------
//
//  <-- NO CHECK ERROR OUTPUT -->
///shared x
 x = [-2, -1, 0, 1, 2];
assert_checkalmostequal (moc_polyfit (x, x.^2+x+1, 2), [1, 1, 1], sqrt (%eps));
assert_checkalmostequal (moc_polyfit (x, x.^2+x+1, 3), [0, 1, 1, 1],%eps, sqrt (%eps));
//fail ("moc_polyfit (x, x.^2+x+1)")
//fail ("moc_polyfit (x, x.^2+x+1, [])")
//
// // // test difficult case where scaling is really needed. This example
// // demonstrates the rather poor result which occurs when the dependent
// // variable is not normalized properly.
// // Also check the usage of 2nd & 3rd output arguments.
// test
 x = [ -1196.4, -1195.2, -1194, -1192.8, -1191.6, -1190.4, -1189.2, -1188, ...
       -1186.8, -1185.6, -1184.4, -1183.2, -1182];
 y = [ 315571.7086, 315575.9618, 315579.4195, 315582.6206, 315585.4966, ...
       315588.3172, 315590.9326, 315593.5934, 315596.0455, 315598.4201, ...
       315600.7143, 315602.9508, 315605.1765 ];
 [p1, s1] = moc_polyfit (x, y, 10);
 [p2, s2, mu] = moc_polyfit (x, y, 10);
 assert_checktrue (s2.normr < s1.normr);
//
// test
 x = 1:4;
 p0 = [%i, 0, 2*%i, 4];
 y0 = moc_polyval (p0, x);
 p = moc_polyfit (x, y0, length (p0) - 1);
 assert_checkalmostequal (p,p0,%eps,%eps*10000);

//
// test
 x = 1000 + (-5:5);
 xn = (x - mean (x)) / stdev (x);
 pn = ones (1,5);
 y = moc_polyval (pn, xn);
 [p, s, mu] = moc_polyfit (x, y, length (pn) - 1);
 [p2, s2] = moc_polyfit (x, y, length (pn) - 1);
 assert_checkalmostequal (p, pn, s.normr);
 assert_checkalmostequal (s.yf, y, s.normr);
 assert_checkalmostequal (mu, [mean(x), stdev(x)]);
 assert_checktrue (s.normr/s2.normr < sqrt (%eps));
//
// test
 x = [1, 2, 3; 4, 5, 6];
 y = [0, 0, 1; 1, 0, 0];
 p = moc_polyfit (x, y, 5);
 expected = [0, 1, -14, 65, -112, 60] / 12;
 assert_checkalmostequal (p, expected,%eps,sqrt (%eps));
