<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from moc_confidence.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="moc_confidence" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>moc_confidence</refname>
    <refpurpose>Produce prediction intervals for the fitted y</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [y,dy] = moc_confidence(A,p,s)
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
   <para>
Produce prediction intervals for the fitted y. The vector p
and structure s are returned from wsolve. The matrix A is
the set of observation values at which to evaluate the
confidence interval.
   </para>
   <para>
moc_confidence(...,['ci'|'pi'])
   </para>
   <para>
Produce a confidence interval (range of likely values for the
mean at x) or a prediction interval (range of likely values
seen when measuring at x). The prediction interval tells
you the width of the distribution at x. This should be the same
regardless of the number of measurements you have for the value
at x. The confidence interval tells you how well you know the
mean at x. It should get smaller as you increase the number of
measurements. Error bars in the physical sciences usually show
a 1-alpha confidence value of erfc(1/sqrt(2)), representing
one standandard deviation of uncertainty in the mean.
   </para>
   <para>
moc_confidence(...,1-alpha)
   </para>
   <para>
Control the width of the interval. If asking for the prediction
interval 'pi', the default is .05 for the 95% prediction interval.
If asking for the confidence interval 'ci', the default is
erfc(1/sqrt(2)) for a one standard deviation confidence interval.
   </para>
   <para>
Confidence intervals for linear system are given by:
x' p +/- sqrt( Finv(1-a,1,df) var(x' p) )
where for confidence intervals,
var(x' p) = sigma^2 (x' inv(A'A) x)
and for prediction intervals,
var(x' p) = sigma^2 (1 + x' inv(A'A) x)
   </para>
   <para>
Rather than A'A we have R from the QR decomposition of A, but
R'R equals A'A. Note that R is not upper triangular since we
have already multiplied it by the permutation matrix, but it
is invertible. Rather than forming the product R'R which is
ill-conditioned, we can rewrite x' inv(A'A) x as the equivalent
x' inv(R) inv(R') x = t t', for t = x' inv(R)
Since x is a vector, t t' is the inner product moc_sumsq(t).
Note that LAPACK allows us to do this simultaneously for many
different x using sqrt(moc_sumsq(X/R,2)), with each x on a different row.
   </para>
   <para>
Note: sqrt(F(1-a;1,df)) = T(1-a/2;df)
   </para>
   <para>
For non-linear systems, use x = dy/dp and ignore the y output.
</para>
</refsection>
</refentry>
